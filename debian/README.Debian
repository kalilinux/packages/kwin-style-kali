kwin-style-kali is a fork of kwin-style-breeze with modifications to fit Kali
Linux look and feel.

Here is some guidance on how to rebase the fork on a latest version of
kwin-style-breeze. Let's assume that we'd like to rebase on the tag v6.2.5.

Let's start by fetching a new upstream tag in a temporary directory:

	TAG=v6.2.5
	mkdir breeze-tmp && cd breeze-tmp
	wget https://github.com/KDE/breeze/archive/refs/tags/${TAG:?}.tar.gz -O- \
	  | tar --strip-components=1 -xz

Now let's remove the parts we don't need:

	shopt -s extglob # `setopt ksh_glob` for zsh
	rm -fr !(AUTHORS|CMakeLists.txt|kdecoration|libbreezecommon|LICENSES)

Now sync that with the `breeze` branch of the kwin-style-kali git repo:

	cd kwin-style-kali
	git checkout breeze
	rsync -a --cvs-exclude --delete <<PATH-TO>>/breeze-tmp/ .
	git add .
	git commit -m"Import KDE breeze ${TAG:?}"

Now let's merge the `breeze` branch in the `kali/master` branch:

	git checkout kali/master
	git merge breeze

And now is the time for fun. Fix merge conflicts, work, build, test, commit.
When we're done, let's remember to push both branches.
